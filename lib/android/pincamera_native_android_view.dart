import 'package:flutter/foundation.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/services.dart';

class PinCameraNativeAndroidView extends StatelessWidget {
  const PinCameraNativeAndroidView({
    Key? key,
    this.enableFlipCamera = false,
  }) : super(key: key);

  final bool enableFlipCamera;

  @override
  Widget build(BuildContext context) {
    const String viewType = 'PinCameraNativeAndroidView';
    final Map<String, dynamic> creationParams = <String, dynamic>{};

    return AndroidView(
      viewType: viewType,
      layoutDirection: TextDirection.ltr,
      creationParams: creationParams,
      creationParamsCodec: const StandardMessageCodec(),
    );
  }
}
