import 'package:flutter/foundation.dart';
import 'package:flutter/services.dart';
import 'package:flutter/widgets.dart';
import 'package:pincamera/pincamera.dart';

class PinCameraNativeIOSView extends StatelessWidget {
  const PinCameraNativeIOSView({
    Key? key,
    this.enableFlipCamera = false,
  }) : super(key: key);

  final bool enableFlipCamera;

  @override
  Widget build(BuildContext context) {
    const String viewType = 'PinCameraNativeIOSView';
    final Map<String, dynamic> creationParams = <String, dynamic>{
      'enableFlipCamera': enableFlipCamera ? 'true' : 'false',
    };

    return UiKitView(
      viewType: viewType,
      layoutDirection: TextDirection.ltr,
      creationParams: creationParams,
      creationParamsCodec: const StandardMessageCodec(),
    );
  }
}
