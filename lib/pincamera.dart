import 'dart:async';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:pincamera/android/pincamera_native_android_view.dart';
import 'package:pincamera/ios/pincamera_native_ios_view.dart';

class Pincamera {
  static const MethodChannel _channel = MethodChannel('pincamera');

  static Future<String?> get platformVersion async {
    final String? version = await _channel.invokeMethod('getPlatformVersion');
    return version;
  }

  static Future<String?> get captureImage async {
    final String? result = await _channel.invokeMethod('captureImage');
    return result;
  }

  static Future<String?> get saveImage async {
    final String? result = await _channel.invokeMethod('saveImage');
    return result;
  }

  static Future<String?> get dispose async {
    final String? result = await _channel.invokeMethod('dispose');
    return result;
  }
}

class PinCameraNativeView extends StatefulWidget {
  PinCameraNativeView({
    Key? key,
    required this.controller,
    this.enableFlipCamera = false,
  }) : super(key: key);

  final PinCameraController controller;
  final bool enableFlipCamera;

  @override
  _PinCameraNativeViewState createState() => _PinCameraNativeViewState();
}

class _PinCameraNativeViewState extends State<PinCameraNativeView> {
  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;
    switch (defaultTargetPlatform) {
      case TargetPlatform.android:
        return SizedBox(
          width: width,
          height: height,
          child: PinCameraNativeAndroidView(
            enableFlipCamera: widget.enableFlipCamera,
          ),
        );
      case TargetPlatform.iOS:
        return SizedBox(
          width: width,
          height: height,
          child: PinCameraNativeIOSView(
            enableFlipCamera: widget.enableFlipCamera,
          ),
        );
      default:
        throw UnsupportedError('Unsupported platform view');
    }
  }

  @override
  void dispose() {
    widget.controller.dispose();
    super.dispose();
  }
}

class PinCameraController {
  Future<String?> capture() async {
    String? result = await Pincamera.captureImage;
    return result;
  }

  Future<String?> saveImage() async {
    String? result = await Pincamera.saveImage;
    print('saveimage result: $result');
    return result;
  }

  Future<void> dispose() async {
    await Pincamera.dispose;
  }
}
