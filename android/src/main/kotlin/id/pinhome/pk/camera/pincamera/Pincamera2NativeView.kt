package id.pinhome.pk.camera.pincamera

import android.Manifest
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.Color
import android.graphics.ImageFormat
import android.graphics.SurfaceTexture
import android.hardware.camera2.*
import android.media.Image
import android.media.ImageReader
import android.os.Build
import android.os.Environment
import android.os.Handler
import android.os.HandlerThread
import android.transition.TransitionManager
import android.util.Log
import android.util.Size
import android.util.SparseIntArray
import android.view.Surface
import android.view.TextureView
import android.view.View
import android.view.WindowManager
import android.widget.Toast
import androidx.annotation.NonNull
import androidx.annotation.RequiresApi
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.constraintlayout.widget.ConstraintSet
import androidx.constraintlayout.widget.ConstraintSet.*
import androidx.core.content.ContextCompat
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LifecycleRegistry
import id.pinhome.pk.camera.pincamera.activities.ACTION_PERMISSIONS_DENIED
import id.pinhome.pk.camera.pincamera.activities.ACTION_PERMISSIONS_GRANTED
import id.pinhome.pk.camera.pincamera.activities.PermissionActivity
import io.flutter.plugin.common.MethodChannel
import io.flutter.plugin.common.StandardMessageCodec
import io.flutter.plugin.platform.PlatformView
import io.flutter.plugin.platform.PlatformViewFactory
import java.io.File
import java.io.FileOutputStream
import java.net.URI
import java.text.SimpleDateFormat
import java.util.*

interface PincameraInterface {
    fun isActive(): Boolean
    fun dispose()
    fun captureImage()
    fun saveImage(result: MethodChannel.Result)
}

class PincameraNativeViewFactory : PlatformViewFactory(StandardMessageCodec.INSTANCE) {
    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
    override fun create(context: Context, viewId: Int, args: Any?): PlatformView {
        val creationParams = args as Map<String?, Any?>?
        return PincameraNativeView(context, viewId, creationParams)
    }
}

@RequiresApi(Build.VERSION_CODES.LOLLIPOP)
class PincameraNativeView(
        val context: Context,
        id: Int,
        creationParams: Map<String?, Any?>?
) : PlatformView, BroadcastReceiver(), LifecycleOwner, TextureView.SurfaceTextureListener, PincameraInterface {
    private val lifecycle: LifecycleRegistry
    private val container: ConstraintLayout
    private val textureView: TextureView

    private var state: Int = 0
    private var isCameraReady: Boolean = false
    private val windowManager: WindowManager
    private var camera: CameraDevice? = null
    private var cameraManager: CameraManager? = null
    private var captureRequest: CaptureRequest? = null
    private var captureRequestBuilder: CaptureRequest.Builder? = null
    private val cameraStateCallback = object : CameraDevice.StateCallback() {
        override fun onOpened(cam: CameraDevice) {
            Log.d("Pincamera", "camera opened")
            camera = cam
            isCameraReady = true

            buildViewFinderAndCaptureSession()
        }

        override fun onDisconnected(cam: CameraDevice) {
            Log.d("Pincamera", "camera disconnected")
            cam.close()
            camera = null
            isCameraReady = false
        }

        override fun onError(cam: CameraDevice, error: Int) {
            Log.d("Pincamera", "camera error: $error")
            cam.close()
            camera = null
            isCameraReady = false
            state = STATE_PREVIEW
        }

        override fun onClosed(camera: CameraDevice) {
            super.onClosed(camera)
            Log.d("Pincamera", "camera closed")
        }
    }
    private var cameraCaptureSession: CameraCaptureSession? = null
    private var captureSessionCallback = object : CameraCaptureSession.CaptureCallback() {
//        private fun process(result: CaptureResult) {
//            when (state) {
//                STATE_PREVIEW -> {
//                    // do nothing
//                }
//                STATE__READY_TO_CAPTURE -> {
//                    captureStillImage()
//                }
//                STATE__CAPTURE -> {
//                    print("capturing image")
//                }
//                else -> {
//                }
//            }
//        }

        override fun onCaptureStarted(session: CameraCaptureSession, request: CaptureRequest, timestamp: Long, frameNumber: Long) {
            super.onCaptureStarted(session, request, timestamp, frameNumber)
            Log.d("Pincamera", "captureSessionCallback: onCaptureStarted")
        }

        override fun onCaptureCompleted(session: CameraCaptureSession, request: CaptureRequest, result: TotalCaptureResult) {
            super.onCaptureCompleted(session, request, result)
            Log.d("Pincamera", "captureSessionCallback: onCaptureCompleted")

//            process(result)
        }

        override fun onCaptureFailed(session: CameraCaptureSession, request: CaptureRequest, failure: CaptureFailure) {
            super.onCaptureFailed(session, request, failure)
            Log.d("Pincamera", "captureSessionCallback: onCaptureFailed")
        }

    }
    private var previewSize: Size? = null
    private var backgroundHandlerThread: HandlerThread? = null
    private var backgroundHandler: Handler? = null

    private var imageFileLocation: String = ""
    private var image: Image? = null
    private var requestingAppURI: URI? = null
    private var imageReader: ImageReader? = null
    private var onImageReadyListener: ImageReader.OnImageAvailableListener = object : ImageReader.OnImageAvailableListener {
        override fun onImageAvailable(reader: ImageReader?) {
            Log.d("Pincamera", "image available")
            if (reader == null) return
//            backgroundHandler?.post(ImageSaver(reader.acquireNextImage()))
            state = STATE_PREVIEW
        }
    }

    init {
        lifecycle = LifecycleRegistry(this)
        lifecycle.currentState = Lifecycle.State.INITIALIZED

        container = ConstraintLayout(context)
        container.id = View.generateViewId()
        container.setBackgroundColor(Color.BLUE)

        textureView = TextureView(context)
        textureView.id = View.generateViewId()
        container.addView(textureView)

        windowManager = context.getSystemService(Context.WINDOW_SERVICE) as WindowManager

        setViewConstraints()
        startBackgroundThread()

        PincameraPlugin.cameraInterface = this
        lifecycle.currentState = Lifecycle.State.CREATED
    }

    private fun setViewConstraints() {
        textureView.layoutParams = ConstraintLayout.LayoutParams(0, 0)
        val constraintSet = ConstraintSet()
        constraintSet.clone(container)

        // align start
        constraintSet.connect(
                textureView.id,
                START,
                container.id,
                START,
                0
        )
        // align end
        constraintSet.connect(
                textureView.id,
                END,
                container.id,
                END,
                0
        )
        // align top
        constraintSet.connect(
                textureView.id,
                TOP,
                container.id,
                TOP,
                0
        )
        // align bottom
        constraintSet.connect(
                textureView.id,
                BOTTOM,
                container.id,
                BOTTOM,
                0
        )
        constraintSet.constrainDefaultWidth(textureView.id, MATCH_CONSTRAINT_SPREAD)
        constraintSet.constrainDefaultHeight(textureView.id, MATCH_CONSTRAINT_SPREAD)

        // optinally: apply constraints smoothly
        TransitionManager.beginDelayedTransition(container)

        // apply
        constraintSet.applyTo(container)
    }

    private fun requestCameraPermission() {
        // Request camera permissions
        if (allPermissionsGranted()) {
            setupAndOpenCamera()
        } else {
            startRequestPermissionActivity()
        }
    }

    private fun startRequestPermissionActivity() {
        val intentFilter = IntentFilter()
        intentFilter.addAction(ACTION_PERMISSIONS_GRANTED)
        intentFilter.addAction(ACTION_PERMISSIONS_DENIED)
        context.registerReceiver(this, intentFilter)
        var intent = Intent(context, PermissionActivity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
        context.startActivity(intent)
    }

    private fun allPermissionsGranted() = REQUIRED_PERMISSIONS.all {
        ContextCompat.checkSelfPermission(
                context, it) == PackageManager.PERMISSION_GRANTED
    }

    private fun setupAndOpenCamera() {
        // step 1: get gamera id
        var cameraId = ""
        val cameraManager = context.getSystemService(Context.CAMERA_SERVICE) as? CameraManager
        if (cameraManager == null) {
            Log.d("Pincamera", "Failed to get camera manager")
            return
        }
        this.cameraManager = cameraManager

        val cameraIds: Array<String> = cameraManager.cameraIdList
        for (id in cameraIds) {
            val cameraCharacteristics = cameraManager.getCameraCharacteristics(id)
            //If we want to choose the rear facing camera instead of the front facing one
            if (cameraCharacteristics.get(CameraCharacteristics.LENS_FACING) == CameraCharacteristics.LENS_FACING_FRONT) {
                continue
            }

            previewSize = cameraCharacteristics
                    .get(CameraCharacteristics.SCALER_STREAM_CONFIGURATION_MAP)
                    ?.getOutputSizes(ImageFormat.JPEG)
                    ?.maxByOrNull { it.width * it.height } ?: continue

            imageReader = ImageReader.newInstance(
                    previewSize!!.width,
                    previewSize!!.height,
                    ImageFormat.JPEG,
                    1)
            imageReader!!.setOnImageAvailableListener(onImageReadyListener, backgroundHandler)
            cameraId = id
            break
        }

        try {
            cameraManager.openCamera(cameraId, cameraStateCallback, backgroundHandler)
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    private fun buildViewFinderAndCaptureSession() {
        val imageReader = imageReader ?: return
        val previewSize = previewSize ?: return
        val cam = this.camera ?: return

        val surfaceTexture: SurfaceTexture = textureView.surfaceTexture ?: return
        surfaceTexture.setDefaultBufferSize(previewSize.width, previewSize.height) //3
        val previewSurface = Surface(surfaceTexture)

        cam.createCaptureSession(
                listOf(previewSurface, imageReader.surface),
                object : CameraCaptureSession.StateCallback() {
                    override fun onConfigured(session: CameraCaptureSession) {
                        Log.d("Pincamera", "on configured")
                        if (camera == null) return

                        val captureRequestBuilder = cam.createCaptureRequest(CameraDevice.TEMPLATE_PREVIEW) //4
                        captureRequestBuilder.addTarget(previewSurface)

                        try {
                            cameraCaptureSession = session
                            cameraCaptureSession!!.setRepeatingRequest(
                                    captureRequestBuilder.build(),
                                    captureSessionCallback,
                                    backgroundHandler)
                        } catch (e: Exception) {
                            e.printStackTrace()
                        }
                    }

                    override fun onConfigureFailed(session: CameraCaptureSession) {
                        Log.d("Pincamera", "on configure failed")
                    }

                    override fun onReady(session: CameraCaptureSession) {
                        super.onReady(session)
                        cameraCaptureSession = session
                        if (state == STATE__PREPARE_TO_CAPTURE) {
                            state = STATE__READY_TO_CAPTURE
                            captureStillImage(session)
                        }
                    }

                    override fun onActive(session: CameraCaptureSession) {
                        super.onActive(session)
                        cameraCaptureSession = session
                        if (state == STATE__READY_TO_CAPTURE) {
                            state = STATE__CAPTURE
                        }
                    }

                    override fun onCaptureQueueEmpty(session: CameraCaptureSession) {
                        super.onCaptureQueueEmpty(session)
                    }

                    override fun onClosed(session: CameraCaptureSession) {
                        super.onClosed(session)
                    }

                    override fun onSurfacePrepared(session: CameraCaptureSession, surface: Surface) {
                        super.onSurfacePrepared(session, surface)
                    }
                },
                backgroundHandler) //6
    }

    private fun startBackgroundThread() {
        backgroundHandlerThread = HandlerThread("CameraVideoThread")
        backgroundHandlerThread!!.start()
        backgroundHandler = Handler(backgroundHandlerThread!!.looper)
    }

    private fun stopBackgroundThread() {
        backgroundHandlerThread?.quitSafely()
        try {
            backgroundHandlerThread?.join()
            backgroundHandlerThread = null
            backgroundHandler = null
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    override fun getView(): View {
        return container
    }

    override fun isActive(): Boolean {
        return lifecycle.currentState == Lifecycle.State.STARTED
    }

    override fun dispose() {
        lifecycle.currentState = Lifecycle.State.DESTROYED
        closeCamera()
        stopBackgroundThread()
    }

    private fun closeCamera() {
        isCameraReady = false

        cameraCaptureSession?.close()
        cameraCaptureSession = null

        camera?.close()
        camera = null

        imageReader?.close()
        imageReader = null
    }

    override fun captureImage() {
        try {
            imageFile = if (requestingAppURI != null) {
                File(requestingAppURI!!.path)
            } else {
                createImageFile()
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }

        state = STATE__READY_TO_CAPTURE

        val cameraCaptureSession = cameraCaptureSession ?: return
        captureStillImage(cameraCaptureSession)
//        lockFocus()
    }

    private fun prepareToCapture() {
        state = STATE__PREPARE_TO_CAPTURE
        val cameraCaptureSession = this.cameraCaptureSession ?: return
        cameraCaptureSession.abortCaptures()
    }

    private fun captureStillImage(session: CameraCaptureSession) {
        if (state != STATE__READY_TO_CAPTURE) return

        Log.d("Pincamera", "capture image")
        val camera = this.camera ?: return
        val imageReader = this.imageReader ?: return
        val captureRequestBuilder = camera.createCaptureRequest(CameraDevice.TEMPLATE_STILL_CAPTURE)
        captureRequestBuilder.addTarget(imageReader.surface)

//        val rotation = windowManager.defaultDisplay.rotation
//        captureRequestBuilder.set(CaptureRequest.JPEG_ORIENTATION, orientations.get(rotation))

        val captureCallback = object : CameraCaptureSession.CaptureCallback() {
            override fun onCaptureStarted(session: CameraCaptureSession, request: CaptureRequest, timestamp: Long, frameNumber: Long) {
                super.onCaptureStarted(session, request, timestamp, frameNumber)
//                try {
//                    imageFile = if (requestingAppURI != null) {
//                        File(requestingAppURI!!.path)
//                    } else {
//                        createImageFile()
//                    }
//                } catch (e: Exception) {
//                    print("error create image file $e")
//                }

                Log.d("Pincamera", "captureStillImage: onCaptureStarted")
                state = STATE__CAPTURE
//                unlockFocus()
            }

            override fun onCaptureProgressed(session: CameraCaptureSession, request: CaptureRequest, partialResult: CaptureResult) {
                super.onCaptureProgressed(session, request, partialResult)
            }

            override fun onCaptureCompleted(session: CameraCaptureSession, request: CaptureRequest, result: TotalCaptureResult) {
                super.onCaptureCompleted(session, request, result)
//                captureResult = result
//                unlockFocus()
                Log.d("Pincamera", "captureStillImage: onCaptureComplete")
                if (state == STATE__CAPTURE) {
                    val bitmap = textureView.bitmap
                    if (bitmap == null) {
                        state = STATE_PREVIEW
                        return
                    }
                    backgroundHandler?.post(ImageSaver(bitmap))
                    state = STATE__CAPTURE_COMPLETE
                }
            }

            override fun onCaptureFailed(session: CameraCaptureSession, request: CaptureRequest, failure: CaptureFailure) {
                super.onCaptureFailed(session, request, failure)
            }

            override fun onCaptureSequenceCompleted(session: CameraCaptureSession, sequenceId: Int, frameNumber: Long) {
                super.onCaptureSequenceCompleted(session, sequenceId, frameNumber)
            }

            override fun onCaptureSequenceAborted(session: CameraCaptureSession, sequenceId: Int) {
                super.onCaptureSequenceAborted(session, sequenceId)
            }

            override fun onCaptureBufferLost(session: CameraCaptureSession, request: CaptureRequest, target: Surface, frameNumber: Long) {
                super.onCaptureBufferLost(session, request, target, frameNumber)
            }
        }
//        session.captureSingleRequest(
//                captureRequestBuilder.build(),
//                {
//                    val image = imageReader.acquireLatestImage()
//                    ImageSaver(image)
//                },
//                captureCallback)
        session.capture(
                captureRequestBuilder.build(),
                captureCallback,
                backgroundHandler)
    }

    override fun saveImage(@NonNull result: MethodChannel.Result) {
        Log.d("Pincamera", "save image")
        result.success(imageFileLocation)
    }

    override fun onFlutterViewAttached(flutterView: View) {
        super.onFlutterViewAttached(flutterView)
        lifecycle.currentState = Lifecycle.State.STARTED

        if (textureView.isAvailable) {
            if (!isCameraReady) {
                setupAndOpenCamera()
            }
        } else {
            textureView.surfaceTextureListener = this
        }
    }

    private fun onPermissionsResult() {
        if (allPermissionsGranted()) {
            if (!isCameraReady) {
                setupAndOpenCamera()
            }
        } else {
            Toast.makeText(context,
                    "Permissions not granted by the user.",
                    Toast.LENGTH_SHORT).show()
            dispose()
        }
    }

    private fun createImageFile(): File? {
        val storageDirectory: File = context
                .getExternalFilesDir(Environment.DIRECTORY_PICTURES)
                ?: return null

        val mGalleryFolder = File(storageDirectory, "pincamera/image")
        if (!mGalleryFolder.exists()) {
            mGalleryFolder.mkdirs()
        }

        val timeStamp: String = SimpleDateFormat("yyyyMMdd_HHmmss").format(Date())
        val imageFileName = "IMAGE_" + timeStamp + "_"

        val image = File.createTempFile(imageFileName, ".jpg", mGalleryFolder)
        imageFileLocation = image.absolutePath

        return image
    }
//
//    private fun lockFocus() {
//        val captureRequestBuilder = captureRequestBuilder ?: return
//        val cameraCaptureSession = cameraCaptureSession ?: return
//
//        state = STATE__WAIT_LOCK
//
//        try {
//            captureRequestBuilder.set(
//                    CaptureRequest.CONTROL_AF_TRIGGER,
//                    CaptureRequest.CONTROL_AF_TRIGGER_START)
//
//            cameraCaptureSession.capture(
//                    captureRequestBuilder.build(),
//                    captureSessionCallback,
//                    backgroundHandler)
//        } catch (e: Exception) {
//            print(e)
//        }
//    }
//
//    private fun unlockFocus() {
//        val captureRequestBuilder = captureRequestBuilder ?: return
//        val cameraCaptureSession = cameraCaptureSession ?: return
//
//        state = STATE_PREVIEW
//
//        try {
//            captureRequestBuilder.set(
//                    CaptureRequest.CONTROL_AF_TRIGGER,
//                    CaptureRequest.CONTROL_AF_TRIGGER_CANCEL)
//
//            cameraCaptureSession.capture(
//                    captureRequestBuilder.build(),
//                    captureSessionCallback,
//                    backgroundHandler)
//        } catch (e: Exception) {
//            print(e)
//        }
//    }

    override fun onReceive(context: Context, intent: Intent) {
        when {
            intent.action == ACTION_PERMISSIONS_GRANTED -> {
                context.unregisterReceiver(this)
                onPermissionsResult()
            }
            intent.action == ACTION_PERMISSIONS_DENIED -> {
                context.unregisterReceiver(this)
                dispose()
                return
            }
            intent.action == ACTION_DISPOSE -> {
                context.unregisterReceiver(this)
                dispose()
                return
            }
            intent.action == ACTION_CAPTURE_IMAGE -> {

            }
            intent.action == ACTION_SAVE_IMAGE -> {

            }
        }
    }

    override fun getLifecycle(): Lifecycle {
        return lifecycle
    }

    override fun onSurfaceTextureAvailable(surface: SurfaceTexture, width: Int, height: Int) {
        Log.d("Pincamera", "on surface texture available")

        if (textureView.surfaceTexture == null) {
            textureView.setSurfaceTexture(surface)
        }

        if (!isCameraReady) {
            requestCameraPermission()
        }
    }

    override fun onSurfaceTextureSizeChanged(surface: SurfaceTexture, width: Int, height: Int) {
        Log.d("Pincamera", "on surface texture size changed")
    }

    override fun onSurfaceTextureDestroyed(surface: SurfaceTexture): Boolean {
        Log.d("Pincamera", "on surface texture destroyed")
        return false
    }

    override fun onSurfaceTextureUpdated(surface: SurfaceTexture) {
        Log.d("Pincamera", "on surface texture updated")
    }

    companion object {
        private const val TAG = "PincameraNativeView"
        private const val FILENAME_FORMAT = "yyyy-MM-dd-HH-mm-ss-SSS"

        private const val STATE_PREVIEW = 0
        private const val STATE__PREPARE_TO_CAPTURE = 1
        private const val STATE__READY_TO_CAPTURE = 2
        private const val STATE__CAPTURE = 3
        private const val STATE__CAPTURE_COMPLETE = 4

        const val ACTION_DISPOSE = "PermissionActivity.dispose"
        const val ACTION_CAPTURE_IMAGE = "PermissionActivity.capture_image"
        const val ACTION_SAVE_IMAGE = "PermissionActivity.save_image"

        const val REQUEST_CODE_PERMISSIONS = 10
        val REQUIRED_PERMISSIONS =
                mutableListOf(
                        Manifest.permission.CAMERA,
                        Manifest.permission.RECORD_AUDIO
                ).apply {
                    if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.P) {
                        add(Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    }
                }.toTypedArray()
        val orientations: SparseIntArray = SparseIntArray(4).apply {
            append(Surface.ROTATION_0, 90)
            append(Surface.ROTATION_90, 0)
            append(Surface.ROTATION_180, 270)
            append(Surface.ROTATION_270, 180)
        }


        private var imageFile: File? = null

        private class ImageSaver(val bitmap: Bitmap) : Runnable {

            override fun run() {
                var outStream: FileOutputStream? = null

                try {
                    outStream = FileOutputStream(imageFile)
                    bitmap.compress(Bitmap.CompressFormat.JPEG, 90, outStream)
                    outStream.flush()
                    outStream.close()
                } catch (e: Exception) {
                    e.printStackTrace()
                } finally {
                    try {
                        outStream?.close()
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }
            }
        }
    }
}