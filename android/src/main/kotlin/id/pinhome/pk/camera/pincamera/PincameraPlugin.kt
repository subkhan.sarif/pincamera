package id.pinhome.pk.camera.pincamera

import androidx.annotation.NonNull
import androidx.lifecycle.LifecycleObserver
import io.flutter.embedding.engine.plugins.FlutterPlugin
import io.flutter.plugin.common.MethodCall
import io.flutter.plugin.common.MethodChannel
import io.flutter.plugin.common.MethodChannel.MethodCallHandler
import io.flutter.plugin.common.MethodChannel.Result

/** PincameraPlugin */
class PincameraPlugin : FlutterPlugin, MethodCallHandler, LifecycleObserver {
    /// The MethodChannel that will the communication between Flutter and native Android
    ///
    /// This local reference serves to register the plugin with the Flutter Engine and unregister it
    /// when the Flutter Engine is detached from the Activity
    private lateinit var channel: MethodChannel

    override fun onAttachedToEngine(@NonNull flutterPluginBinding: FlutterPlugin.FlutterPluginBinding) {
        flutterPluginBinding
                .platformViewRegistry
                .registerViewFactory(
                        "PinCameraNativeAndroidView",
                        PincameraNativeViewFactory())

        channel = MethodChannel(flutterPluginBinding.binaryMessenger, "pincamera")
        channel.setMethodCallHandler(this)
    }

    override fun onMethodCall(@NonNull call: MethodCall, @NonNull result: Result) {
        when (call.method) {
            "getPlatformVersion" -> result.success("Android ${android.os.Build.VERSION.RELEASE}")
            "captureImage" -> {
                if (cameraInterface?.isActive() != true) {
                    result.error("100", "camera interface is not active", null)
                    return
                }
                cameraInterface?.captureImage()
                result.success(null)
            }
            "saveImage" -> {
                if (cameraInterface?.isActive() != true) {
                    result.error("200", "camera interface is not active", null)
                    return
                }
                cameraInterface?.saveImage(result)
            }
            "dispose" -> {
                if (cameraInterface?.isActive() != true) {
                    result.error("300", "camera interface is not active", null)
                    return
                }
                cameraInterface?.dispose()
                result.success(null)
            }
            else -> result.notImplemented()
        }
    }

    override fun onDetachedFromEngine(@NonNull binding: FlutterPlugin.FlutterPluginBinding) {
        channel.setMethodCallHandler(null)
    }

    companion object {
        var cameraInterface: PincameraInterface? = null
    }
}
