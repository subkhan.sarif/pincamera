//package id.pinhome.pk.camera.pincamera
//
//import android.Manifest
//import android.content.*
//import android.content.pm.PackageManager
//import android.graphics.Color
//import android.os.Build
//import android.provider.MediaStore
//import android.transition.TransitionManager
//import android.util.Log
//import android.view.View
//import android.widget.Toast
//import androidx.annotation.RequiresApi
//import androidx.camera.core.CameraSelector
//import androidx.camera.core.ImageCapture
//import androidx.camera.core.ImageCapture.CAPTURE_MODE_MAXIMIZE_QUALITY
//import androidx.camera.core.ImageCaptureException
//import androidx.camera.core.Preview
//import androidx.camera.lifecycle.ProcessCameraProvider
//import androidx.camera.view.PreviewView
//import androidx.constraintlayout.widget.ConstraintLayout
//import androidx.constraintlayout.widget.ConstraintSet
//import androidx.constraintlayout.widget.ConstraintSet.*
//import androidx.core.content.ContextCompat
//import androidx.lifecycle.Lifecycle
//import androidx.lifecycle.LifecycleOwner
//import androidx.lifecycle.LifecycleRegistry
//import id.pinhome.pk.camera.pincamera.activities.ACTION_PERMISSIONS_DENIED
//import id.pinhome.pk.camera.pincamera.activities.ACTION_PERMISSIONS_GRANTED
//import id.pinhome.pk.camera.pincamera.activities.PermissionActivity
//import io.flutter.plugin.common.StandardMessageCodec
//import io.flutter.plugin.platform.PlatformView
//import io.flutter.plugin.platform.PlatformViewFactory
//import java.text.SimpleDateFormat
//import java.util.*
//import java.util.concurrent.ExecutorService
//import java.util.concurrent.Executors
//import java.util.concurrent.ThreadFactory
//
//interface PincameraInterface {
//    fun isActive(): Boolean
//    fun dispose()
//    fun captureImage()
//    fun saveImage()
//}
//
//class PincameraNativeViewFactory : PlatformViewFactory(StandardMessageCodec.INSTANCE) {
//    @RequiresApi(Build.VERSION_CODES.LOLLIPOP)
//    override fun create(context: Context, viewId: Int, args: Any?): PlatformView {
//        val creationParams = args as Map<String?, Any?>?
//        return PincameraNativeView(context, viewId, creationParams)
//    }
//}
//
//@RequiresApi(Build.VERSION_CODES.LOLLIPOP)
//class PincameraNativeView(
//        val context: Context,
//        id: Int,
//        creationParams: Map<String?, Any?>?
//) : PlatformView, BroadcastReceiver(), LifecycleOwner, PincameraInterface {
//    private val lifecycle: LifecycleRegistry
//    private val container: ConstraintLayout
//    private val viewFinder: PreviewView
//    private val cameraExecutor: ExecutorService
//    private var imageCapture: ImageCapture? = null
//
//    init {
//        lifecycle = LifecycleRegistry(this)
//        lifecycle.currentState = Lifecycle.State.INITIALIZED
//
//        container = ConstraintLayout(context)
//        container.id = View.generateViewId()
//        container.setBackgroundColor(Color.BLUE)
//
//        viewFinder = PreviewView(context)
//        viewFinder.id = View.generateViewId()
//        viewFinder.setBackgroundColor(Color.rgb(255, 255, 255))
//
//        container.addView(viewFinder)
//        setViewConstraints()
//
//        cameraExecutor = Executors.newSingleThreadExecutor()
//
//        initiateCameraView()
//
//        lifecycle.currentState = Lifecycle.State.CREATED
//
//        PincameraPlugin.cameraInterface = this
//    }
//
//    private fun setViewConstraints() {
//        viewFinder.layoutParams = ConstraintLayout.LayoutParams(0, 0)
//        val constraintSet = ConstraintSet()
//        constraintSet.clone(container)
//
//        // align start
//        constraintSet.connect(
//                viewFinder.id,
//                START,
//                container.id,
//                START,
//                0
//        )
//        // align end
//        constraintSet.connect(
//                viewFinder.id,
//                END,
//                container.id,
//                END,
//                0
//        )
//        // align top
//        constraintSet.connect(
//                viewFinder.id,
//                TOP,
//                container.id,
//                TOP,
//                0
//        )
//        // align bottom
//        constraintSet.connect(
//                viewFinder.id,
//                BOTTOM,
//                container.id,
//                BOTTOM,
//                0
//        )
//        constraintSet.constrainDefaultWidth(viewFinder.id, MATCH_CONSTRAINT_SPREAD)
//        constraintSet.constrainDefaultHeight(viewFinder.id, MATCH_CONSTRAINT_SPREAD)
//
//        // optinally: apply constraints smoothly
//        TransitionManager.beginDelayedTransition(container)
//
//        // apply
//        constraintSet.applyTo(container)
//    }
//
//    private fun initiateCameraView() {
//        requestCameraPermission()
//
//    }
//
//    private fun requestCameraPermission() {
//        // Request camera permissions
//        if (allPermissionsGranted()) {
//            startCamera()
//        } else {
//            startRequestPermissionActivity()
//        }
//    }
//
//    private fun startRequestPermissionActivity() {
//        val intentFilter = IntentFilter()
//        intentFilter.addAction(ACTION_PERMISSIONS_GRANTED)
//        intentFilter.addAction(ACTION_PERMISSIONS_DENIED)
//        context.registerReceiver(this, intentFilter)
//        var intent = Intent(context, PermissionActivity::class.java)
//        intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
//        context.startActivity(intent)
//    }
//
//    private fun allPermissionsGranted() = REQUIRED_PERMISSIONS.all {
//        ContextCompat.checkSelfPermission(
//                context, it) == PackageManager.PERMISSION_GRANTED
//    }
//
//    private fun startCamera() {
//        val cameraProviderFuture = ProcessCameraProvider.getInstance(context)
//
//        cameraProviderFuture.addListener(Runnable {
//            // Used to bind the lifecycle of cameras to the lifecycle owner
//            val cameraProvider: ProcessCameraProvider = cameraProviderFuture.get()
//
//            // Preview
//            val preview = Preview.Builder()
//                    .build()
//                    .also {
//                        it.setSurfaceProvider(viewFinder.surfaceProvider)
//                    }
//
//            val builder = ImageCapture.Builder()
//            builder.setCaptureMode(CAPTURE_MODE_MAXIMIZE_QUALITY)
//            imageCapture = builder.build()
//
//            // Select back camera as a default
//            val cameraSelector = CameraSelector.DEFAULT_BACK_CAMERA
//
//            try {
//                // Unbind use cases before rebinding
//                cameraProvider.unbindAll()
//
//                // Bind use cases to camera
//                cameraProvider.bindToLifecycle(
//                        this,
//                        cameraSelector,
//                        preview,
//                        imageCapture)
//            } catch (exc: Exception) {
//                Log.e(TAG, "Use case binding failed", exc)
//            }
//
//        }, ContextCompat.getMainExecutor(context))
//    }
//
//    override fun getView(): View {
//        return container
//    }
//
//    override fun isActive(): Boolean {
//        return lifecycle.currentState == Lifecycle.State.STARTED
//    }
//
//    override fun dispose() {
//        lifecycle.currentState = Lifecycle.State.DESTROYED
//        cameraExecutor.shutdown()
//    }
//
//    override fun captureImage() {
//        // Get a stable reference of the modifiable image capture use case
//        val imageCapture = imageCapture ?: return
//
//        // Create time stamped name and MediaStore entry.
//        val name = SimpleDateFormat(FILENAME_FORMAT, Locale.US)
//                .format(System.currentTimeMillis())
//        val contentValues = ContentValues().apply {
//            put(MediaStore.MediaColumns.DISPLAY_NAME, name)
//            put(MediaStore.MediaColumns.MIME_TYPE, "image/jpeg")
//            if (Build.VERSION.SDK_INT > Build.VERSION_CODES.P) {
//                put(MediaStore.Images.Media.RELATIVE_PATH, "pincamera/image")
//            }
//        }
//
//        // Create output options object which contains file + metadata
//        val outputOptions = ImageCapture.OutputFileOptions
//                .Builder(context.contentResolver,
//                        MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
//                        contentValues)
//                .build()
//
//        // Set up image capture listener, which is triggered after photo has
//        // been taken
//        imageCapture.takePicture(
//                outputOptions,
//                cameraExecutor,
//                object : ImageCapture.OnImageSavedCallback {
//                    override fun onError(exc: ImageCaptureException) {
//                        Log.e(TAG, "Photo capture failed: ${exc.message}", exc)
//                    }
//
//                    override fun onImageSaved(output: ImageCapture.OutputFileResults) {
//                        val msg = "Photo capture succeeded: ${output.savedUri}"
//                        Toast.makeText(context, msg, Toast.LENGTH_SHORT).show()
//                        Log.d(TAG, msg)
//                    }
//                }
//        )
//    }
//
//    override fun saveImage() {
//        print("save image")
//    }
//
//    override fun onFlutterViewAttached(flutterView: View) {
//        super.onFlutterViewAttached(flutterView)
//        lifecycle.currentState = Lifecycle.State.STARTED
//    }
//
//    private fun onPermissionsResult() {
//        if (allPermissionsGranted()) {
//            startCamera()
//        } else {
//            Toast.makeText(context,
//                    "Permissions not granted by the user.",
//                    Toast.LENGTH_SHORT).show()
//            dispose()
//        }
//    }
//
//    override fun onReceive(context: Context, intent: Intent) {
//        when {
//            intent.action == ACTION_PERMISSIONS_GRANTED -> {
//                context.unregisterReceiver(this)
//                onPermissionsResult()
//            }
//            intent.action == ACTION_PERMISSIONS_DENIED -> {
//                context.unregisterReceiver(this)
//                dispose()
//                return
//            }
//            intent.action == ACTION_DISPOSE -> {
//                context.unregisterReceiver(this)
//                dispose()
//                return
//            }
//            intent.action == ACTION_CAPTURE_IMAGE -> {
//
//            }
//            intent.action == ACTION_SAVE_IMAGE -> {
//
//            }
//        }
//    }
//
//    override fun getLifecycle(): Lifecycle {
//        return lifecycle
//    }
//
//    companion object {
//        private const val TAG = "PincameraNativeView"
//        private const val FILENAME_FORMAT = "yyyy-MM-dd-HH-mm-ss-SSS"
//
//        const val ACTION_DISPOSE = "PermissionActivity.dispose"
//        const val ACTION_CAPTURE_IMAGE = "PermissionActivity.capture_image"
//        const val ACTION_SAVE_IMAGE = "PermissionActivity.save_image"
//
//        const val REQUEST_CODE_PERMISSIONS = 10
//        val REQUIRED_PERMISSIONS =
//                mutableListOf(
//                        Manifest.permission.CAMERA,
//                        Manifest.permission.RECORD_AUDIO
//                ).apply {
//                    if (Build.VERSION.SDK_INT <= Build.VERSION_CODES.P) {
//                        add(Manifest.permission.WRITE_EXTERNAL_STORAGE)
//                    }
//                }.toTypedArray()
//    }
//
//}