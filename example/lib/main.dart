import 'dart:async';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:pincamera/pincamera.dart';
import 'package:pincamera_example/camera_view.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatefulWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  String _platformVersion = 'Unknown';

  @override
  void initState() {
    super.initState();
    initPlatformState();
  }

  // Platform messages are asynchronous, so we initialize in an async method.
  Future<void> initPlatformState() async {
    String platformVersion;

    try {
      platformVersion =
          await Pincamera.platformVersion ?? 'Unknown platform version';
    } on PlatformException {
      platformVersion = 'Failed to get platform version.';
    }

    // If the widget was removed from the tree while the asynchronous platform
    // message was in flight, we want to discard the reply rather than calling
    // setState to update our non-existent appearance.
    if (!mounted) return;

    setState(() {
      _platformVersion = platformVersion;
    });
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      initialRoute: 'home',
      onGenerateRoute: (RouteSettings settings) {
        switch (settings.name) {
          case 'home':
            return MaterialPageRoute(
              settings: settings,
              builder: (_) {
                return Home();
              },
            );
          case 'camera':
            return MaterialPageRoute(
              settings: settings,
              builder: (_) {
                return CameraView();
              },
            );
        }
      },
    );
  }
}

class Home extends StatefulWidget {
  @override
  HomeState createState() => HomeState();
}

class HomeState extends State<Home> {
  String? result = null;
  File? image;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Plugin example app'),
      ),
      body: Center(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            GestureDetector(
              onTap: () => openCamera(context),
              child: Text('Open Camera'),
            ),
            Text(result ?? ''),
            if (image != null) Image.file(image!),
          ],
        ),
      ),
    );
  }

  Future<void> openCamera(BuildContext context) async {
    final dynamic result = await Navigator.of(context).push<dynamic>(
      MaterialPageRoute<void>(
        builder: (BuildContext context) => CameraView(),
      ),
    );

    print('result: $result\n');
    if (result != null && result is String) {
      setState(() {
        this.result = result;
        image = File(result);
      });
    }
  }
}
