import 'package:flutter/material.dart';
import 'package:pincamera/pincamera.dart';

class CameraView extends StatelessWidget {
  final controller = PinCameraController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        child: Stack(
          children: [
            Positioned(
              top: 0,
              left: 0,
              right: 0,
              child: SizedBox(
                height: 300,
                width: 100,
                child: PinCameraNativeView(
                  controller: controller,
                  enableFlipCamera: true,
                ),
              ),
            ),
            Positioned(
              bottom: 20,
              left: 0,
              right: 0,
              height: 100,
              child: Row(
                children: [
                  GestureDetector(
                    onTap: () => capture(),
                    child: Text("Capture"),
                  ),
                  GestureDetector(
                    onTap: () => saveImage(context),
                    child: Text("Save"),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  Future<void> capture() async {
    final dynamic result = await controller.capture();
    print("result: " + result);
  }

  Future<String?> saveImage(BuildContext context) async {
    final dynamic result = await controller.saveImage();
    print('save image result: $result');
    controller.dispose();
    Navigator.of(context).pop(result);
  }
}
