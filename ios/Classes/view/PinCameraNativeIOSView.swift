//
//  PinCameraNativeIOSView.swift
//  pincamera
//
//  Created by Pinhome on 28/01/22.
//

import Flutter
import UIKit
import AVFoundation

protocol PinCameraNativeIOSDelegate {
    func captureImage()
}

class PinCameraNativeIOSViewFactory: NSObject, FlutterPlatformViewFactory {
    private var messenger: FlutterBinaryMessenger
    
    init(messenger: FlutterBinaryMessenger) {
        self.messenger = messenger
        super.init()
    }
    
    func create(
        withFrame frame: CGRect,
        viewIdentifier viewId: Int64,
        arguments args: Any?
    ) -> FlutterPlatformView {
        if #available(iOS 11.0, *) {
            return PinCameraNativeIOSView(
                frame: frame,
                viewIdentifier: viewId,
                arguments: args,
                binaryMessenger: messenger)
        } else {
            return UnavailableIOSVersionView(
                frame: frame,
                viewIdentifier: viewId,
                arguments: args,
                binaryMessenger: messenger)
        }
    }
    
    public func createArgsCodec() -> FlutterMessageCodec & NSObjectProtocol {
        return FlutterStandardMessageCodec.sharedInstance()
    }
}

@available(iOS 11.0, *)
class PinCameraNativeIOSView: NSObject, FlutterPlatformView {
    private var _view: UIView
    
    var cameraPosition: AVCaptureDevice.Position = .back
    var captureSession: AVCaptureSession?
    var stillImageOutput: AVCapturePhotoOutput?
    var videoPreviewLayer: AVCaptureVideoPreviewLayer?
    
    var previewOverlay: UIView?
    var captureButton: UIButton?
    var arguments: [String: Any]?
    var imageResult: UIImage?
    
    init(
        frame: CGRect,
        viewIdentifier viewId: Int64,
        arguments args: Any?,
        binaryMessenger messenger: FlutterBinaryMessenger?
    ) {
        _view = UIView()
        super.init()
        
        arguments = args as? [String: Any]
        registerListeners()
        buildCameraView(view: _view)
        buildFlipCameraButton()
    }
    
    func view() -> UIView {
        return _view
    }
    
    func registerListeners() {
        EventListener.shared.listenEvent(event: .captureImage) { [weak self] _ in
            self?.captureImage()
        }
        
        EventListener.shared.listenEvent(event: .saveImage) { [weak self] result in
            guard let res = result as? FlutterResult else {
                return
            }
            guard let imageResult = self?.imageResult else {
                res(nil)
                return
            }
            
            let path = self?.saveImage(image: imageResult)
            res(path)
        }
    }
    
    func buildCameraView(view: UIView) {
        // setup session
//        if (captureSession == nil) {
            captureSession = AVCaptureSession()
            captureSession!.sessionPreset = .medium
//        }
        
        guard let camera = AVCaptureDevice.default(.builtInWideAngleCamera, for: .video, position: cameraPosition) else {
            print("Unable to open \(cameraPosition.rawValue) camera")
            return
        }
        
        // guard let backCamera = AVCaptureDevice.default(for: .video)
        
        do {
            let input = try AVCaptureDeviceInput(device: camera)
            stillImageOutput = AVCapturePhotoOutput()
            
            if captureSession!.canAddInput(input) && captureSession!.canAddOutput(stillImageOutput!) {
                captureSession!.addInput(input)
                captureSession!.addOutput(stillImageOutput!)
                setupLivePreview()
            }
        }
        catch let error  {
            print("Error Unable to initialize back camera: \(error.localizedDescription)")
        }
    }
    
    func setupLivePreview() {
        guard let captureSession = captureSession else {
            print("Capture session is nil!")
            return
        }
        
        videoPreviewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
        videoPreviewLayer!.videoGravity = .resizeAspect
        videoPreviewLayer!.connection?.videoOrientation = .portrait
        
        previewOverlay = UIView()
        previewOverlay!.layer.addSublayer(videoPreviewLayer!)
        _view.addSubview(previewOverlay!)
        
        previewOverlay!.translatesAutoresizingMaskIntoConstraints = false
        previewOverlay!.topAnchor.constraint(equalTo: _view.topAnchor).isActive = true
        previewOverlay!.leadingAnchor.constraint(equalTo: _view.leadingAnchor).isActive = true
        previewOverlay!.trailingAnchor.constraint(equalTo: _view.trailingAnchor).isActive = true
        if captureButton != nil {
            previewOverlay!.bottomAnchor.constraint(equalTo: captureButton!.topAnchor).isActive = true
        } else {
            previewOverlay!.bottomAnchor.constraint(equalTo: _view.bottomAnchor).isActive = true
        }
        
        
        DispatchQueue.global(qos: .userInitiated).async {
            captureSession.startRunning()
            DispatchQueue.main.async {
                self.videoPreviewLayer?.frame = self.previewOverlay!.bounds
            }
        }
    }
    
    func buildFlipCameraButton() {
        guard let arguments = arguments else {
            return
        }

        let enableFlipCamera = arguments["enableFlipCamera"] as? String ?? "false"
        
        if enableFlipCamera != "true" {
            return
        }
        
        captureButton = UIButton()
        captureButton!.setTitle("Flip", for: .normal)
        captureButton!.setTitleColor(.black, for: .normal)
        captureButton!.addTarget(self, action: #selector(flipCamera), for: .touchUpInside)
        captureButton!.layer.zPosition = 1000
        captureButton!.translatesAutoresizingMaskIntoConstraints = false
        
        _view.addSubview(captureButton!)
        
        captureButton!.bottomAnchor.constraint(equalTo: _view.bottomAnchor).isActive = true
        captureButton!.leadingAnchor.constraint(equalTo: _view.leadingAnchor).isActive = true
        captureButton!.trailingAnchor.constraint(equalTo: _view.trailingAnchor).isActive = true
        
    }
    
    @objc func captureImage() {
        let settings = AVCapturePhotoSettings(format: [AVVideoCodecKey: AVVideoCodecType.jpeg])
        stillImageOutput?.capturePhoto(with: settings, delegate: self)
    }
    
    @objc func flipCamera() {
        captureSession?.stopRunning()
        videoPreviewLayer?.removeFromSuperlayer()
        
        if cameraPosition == .back {
            cameraPosition = .front
        } else {
            cameraPosition = .back
        }
        buildCameraView(view: _view)
    }
}

@available(iOS 11.0, *)
extension PinCameraNativeIOSView: AVCapturePhotoCaptureDelegate {
    func photoOutput(_ output: AVCapturePhotoOutput, didFinishProcessingPhoto photo: AVCapturePhoto, error: Error?) {
        guard let imageData = photo.fileDataRepresentation()
        else { return }
        
        imageResult = UIImage(data: imageData)
        
        let captureImageView = UIImageView()
        captureImageView.image = imageResult!
        captureImageView.translatesAutoresizingMaskIntoConstraints = false
        
        _view.addSubview(captureImageView)
        
        captureImageView.centerYAnchor.constraint(equalTo: _view.centerYAnchor).isActive = true
        captureImageView.centerXAnchor.constraint(equalTo: _view.centerXAnchor).isActive = true
        
        
        
        stopCaptureSession()
    }
    
    func stopCaptureSession() {
        videoPreviewLayer?.isHidden = true
        captureSession?.stopRunning()
    }
    
    func resumeCaptureSession() {
        videoPreviewLayer?.isHidden = false
        captureSession?.startRunning()
    }
    
    func saveImage(image: UIImage) -> String? {
        let time = Date()
        guard let data = image.jpegData(compressionQuality: 1) ?? image.pngData() else {
            return nil
        }
        guard let directory = try? FileManager.default.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false) as NSURL else {
            return nil
        }
        do {
            let url = directory.appendingPathComponent("\(time.timeIntervalSince1970).jpg")!
            try data.write(to: url)
            return url.path
        } catch {
            print(error.localizedDescription)
            return nil
        }
    }
}


class UnavailableIOSVersionView: NSObject, FlutterPlatformView {
    private var _view: UIView
    
    init(
        frame: CGRect,
        viewIdentifier viewId: Int64,
        arguments args: Any?,
        binaryMessenger messenger: FlutterBinaryMessenger?
    ) {
        _view = UIView()
        super.init()
        
        createNativeView(view: _view)
    }
    
    func view() -> UIView {
        return _view
    }
    
    func createNativeView(view _view: UIView){
        _view.backgroundColor = UIColor.white
        let nativeLabel = UILabel()
        nativeLabel.text = "Your iOS version is not supported.\nPlease update to iOS 10 or later."
        nativeLabel.textColor = UIColor.black
        nativeLabel.numberOfLines = 2
        nativeLabel.textAlignment = .center
        nativeLabel.translatesAutoresizingMaskIntoConstraints = false
        
        _view.addSubview(nativeLabel)
        
        
        nativeLabel.topAnchor.constraint(equalTo: _view.topAnchor).isActive = true
        nativeLabel.bottomAnchor.constraint(equalTo: _view.bottomAnchor).isActive = true
        nativeLabel.leadingAnchor.constraint(equalTo: _view.leadingAnchor).isActive = true
        nativeLabel.trailingAnchor.constraint(equalTo: _view.trailingAnchor).isActive = true
    }
}
