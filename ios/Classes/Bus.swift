//
//  Bus.swift
//  pincamera
//
//  Created by Pinhome on 30/01/22.
//

import Foundation

enum EventName: String {
    case captureImage
    case saveImage
}

class Bus {
    static let instance: Bus = Bus()
    
    func post(eventName: EventName, arguments: Any? = nil, userInfo: [AnyHashable : Any]? = nil) {
        let notification = Notification(name: Notification.Name(eventName.rawValue), object: arguments, userInfo: userInfo)
        NotificationCenter.default.post(notification)
    }
}

class EventListener {
    typealias Callback = (Any?) -> Void
    
    static let shared: EventListener = EventListener()
    
    private var callback: [String: Callback] = [:]
    
    func listenEvent(event: EventName, callback: @escaping Callback) {
        self.callback[event.rawValue] = callback
        self.addObserver(event: event)
    }
    
    private func addObserver(event: EventName) {
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(self.handler),
            name: NSNotification.Name(rawValue: event.rawValue),
            object: nil)
    }
    
    @objc private func handler(notification: Notification) {
        guard let callback = self.callback[notification.name.rawValue] else {
            return
        }
        
        callback(notification.object)
    }
    
    func dispose() {
        self.callback.removeAll()
        NotificationCenter.default.removeObserver(self)
    }
}
