import Flutter
import UIKit

public class SwiftPincameraPlugin: NSObject, FlutterPlugin {
    
    
    
    public static func register(with registrar: FlutterPluginRegistrar) {
        
        // register view PinCameraNativeIOSView
        let factory = PinCameraNativeIOSViewFactory(messenger: registrar.messenger())
        registrar.register(factory, withId: "PinCameraNativeIOSView")
        
        let channel = FlutterMethodChannel(name: "pincamera", binaryMessenger: registrar.messenger())
        let instance = SwiftPincameraPlugin()
        registrar.addMethodCallDelegate(instance, channel: channel)
    }
    
    public func handle(_ call: FlutterMethodCall, result: @escaping FlutterResult) {
        switch call.method {
        case "openCamera":
            result("A simple camera")
            break
        case "captureImage":
            Bus.instance.post(eventName: .captureImage)
            result(nil)
            break
        case "saveImage":
            Bus.instance.post(eventName: .saveImage, arguments: result)
            break
        case "dispose":
            EventListener.shared.dispose()
            result(nil)
            break
        default:
            result(UIDevice.current.systemVersion)
        }
    }
}
